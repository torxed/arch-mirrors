import pytest
import toml
import os

from arch_mirrors.models.versions import VersionParsers

@pytest.fixture
def tier0_mirror():
	return """
[compatability]
version = "v1"

[Tier0]
tier = 0

# In Gb/s
bandwidth = 40
emails = [
	"mirror@archlinux.org"
]

https = [
	"https://archlinux.org/repo/"
]

http = [
	"http://archlinux.org/repo/"
]

rsync = [
	"rsync://archlinux.org/repo/"
]

[Tier1]
tier = 1
upstream = "Tier0"
visible = false

# In Gb/s
bandwidth = 40
emails = [
	"mirror@archlinux.org"
]

https = [
	"https://archlinux.org/repo/"
]

http = [
	"http://archlinux.org/repo/"
]

rsync = [
	"rsync://archlinux.org/repo/"
]

IP = [
	"127.0.0.1"
]
"""

def test_valid_toml(tier0_mirror):
	data = toml.loads(tier0_mirror)

def test_valid_tier0(tier0_mirror):
	VersionParsers['maximum'].value(**toml.loads(tier0_mirror))