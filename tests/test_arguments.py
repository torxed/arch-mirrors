import pytest
import importlib
import pathlib
import sys


original_arguments = [*sys.argv]
class ArgRunner:
	def __init__(self, arguments):
		self.args = arguments

	def __enter__(self):
		sys.argv += self.args
		return self

	def __exit__(self, exc_type, exc_val, exc_tb):
		sys.argv = [*original_arguments]

		with open('/tmp/testing.txt', 'a') as fh:
			fh.write(f"{sys.argv}\n")

	def run(self):
		import arch_mirrors

		with open('/tmp/testing.txt', 'a') as fh:
			fh.write(f"{sys.argv}\n")

		importlib.reload(arch_mirrors)

		arch_mirrors.run_as_a_module()


def test_help():
	try:
		with ArgRunner(['--help']) as instance:
			instance.run()
	except SystemExit as error:
		if error.code != 0:
			raise error

def test_verify():
	with ArgRunner(['verify']) as instance:
		instance.run()

def test_build():
	with ArgRunner(['build']) as instance:
		instance.run()

		assert pathlib.Path('master.toml').exists() is True

def test_build_custom_output():
	with ArgRunner(['build', '--output', 'test_master.toml']) as instance:
		instance.run()

		assert pathlib.Path('test_master.toml').exists() is True

def test_build_custom_source_directory():
	try:
		with ArgRunner(['build', '--output', 'test_master.toml', '--directory', 'dummy_tomls']) as instance:
			instance.run()
	except ValueError:
		# Our dummy_tomls does not exist
		pass