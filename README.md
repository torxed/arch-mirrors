# Mirrors

This repository is currently meant for submitting issues in this repository regarding anything related to the Arch linux mirrors list and questions regarding those. Please submit new mirror requests as defined in the Archwiki https://wiki.archlinux.org/title/DeveloperWiki:NewMirrors. 

Later on there will be guidelines on how to submit new mirror requests and changes via submitting a Gitlab MR
# EXPERIMENTAL - Single source of truth PoC
⚠ This is a proof of concept and very much a work in progress! This is intended to be a PoC for the source of truth for Arch Linux mirrors, managed via Git.

## Versions

Each mirror definition is under version control.
Every version is defined under [specs/](specs/).

The latest [version](VERSION.md) should be used.

## Submission guidelines via Gitlab MRs

New or modifications to existing mirrors must be done via pull requests.
Intended workflow:

1. Clone this repository
2. Add/Modify your TOML file(s) under the respective region
3. Create a Pull Request with your changes
4. Arch Linux mirror admins will merge
5. GitLab will create a new MR merging all new changes into a release MR.
6. Arch Linux mirror admins will merge the release MR

After the final step, the mirror will be read by archweb which will publish the new state of the mirrors.