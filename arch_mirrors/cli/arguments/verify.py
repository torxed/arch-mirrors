import pathlib
from ..args import subparsers
from ..functions.verify import verify

parse_verify = subparsers.add_parser("verify", help="Verifies TOML mirror entries")
parse_verify.add_argument(
	"--version",
	required=False,
	default='maximum',
	nargs='?',
	help="Which version to check TOML files against",
)
parse_verify.add_argument(
	"--directory",
	required=False,
	default=pathlib.Path('./'),
	type=pathlib.Path,
	help="Where to look for TOML files as source information",
)


parse_verify.set_defaults(func=verify)