import tomllib
import tomli_w
from collections import defaultdict

from ...tooling_help import (
	new_defaultdict,
	defaultdict_to_dict,
	tomlify
)

from ...models.versions import VersionParsers


def build(args):
	"""
	Called via :code:`arch-mirrors build`

	Cycles through all mirror definitions and builds a :code:`master.toml`.
	This master file is snapshot of all current mirrors.
	"""

	# We take use of :code:`arch-mirrors verify` function in order
	# to verify all upstream references.
	from .verify import verify_upstreams

	mirrors = defaultdict(new_defaultdict)

	if not args.directory.exists():
		raise ValueError(f"--directory does not exist: {args.directory}")

	for path in args.directory.rglob('*.toml'):
		# We skip the first level to avoid master.toml
		# and pyproject.toml to be read
		if path.parent == args.directory:
			continue

		with path.open('rb') as fh:
			data = tomllib.load(fh)

		if not (version := data.get('compatability', {}).get('version')):
			raise ValueError(f"Mirrors require a [compatability] containing a valid version.")

		country = path.parent
		region = country.parent

		if f"{region}" == ".":
			region = country.name
			country = None
		else:
			region = region.name
			country = country.name

		mirror_object = VersionParsers[version].value(**data)

		if country:
			mirrors[region][country][mirror_object.name] = mirror_object
		else:
			mirrors[region][mirror_object.name] = mirror_object
	
	verify_upstreams(mirrors)

	with args.output.open('wb') as fh:
		tomli_w.dump(tomlify(defaultdict_to_dict(mirrors)), fh)